Quick, Quick, the Most Beautiful Vowel is Voiding

                            based on a painting by Meret Oppenheim, 1934




She was a Valentine, and I was
a Valentine. When the sounds shifted, Meret bled bees
into my mouth. And all the beautiful vowels followed.
Words finally appeared, when the moon entered
his throat. My throat. There are seasons only the dogs know
when they roll their joy through the intoxicating smell
of the dead. Night soil and rain. Yes,
my wife brought me the words
from the painting’s title. Yes, it was painted the year
of my mother’s birth, though she’s reborn now
into the night. Something in me remains
chained to language. Liberated only
by the sound of now. I wish I could speak fire
and have it flame again through the fossil dead.
Had I been Man Ray’s assistant, I would have
surely blushed, offering Meret a robe to cover my misgivings
and all the lustful thoughts that in her
nakedness I try to forget. I try to forget the silk of her armpit
hair, my name embedded in her underarm scar
as the necessary vowel of my mouth. When Dalí leered,
she knew to line the cup and saucer
with fur, reversing the gaze, to protect yet hint at her
most private. Even the spoon shivered a little
with the thought that the moon made words
as violent as love. When we make love, we cook cabbage
or collard greens and call them soup. We search the eelgrass
for the instinctive clotting of ants and know the hair
on our forearms is just as alive, a secret expression
of wind. The earth shakes loose its long strands
of throbbing seaweed, robing the ocean’s shy
trigger fish and an enormous pain
they pump, breathing the coral breaks
at the bottom of the world. When Meret
painted the vowels chained to a place
of indistinct gray hair, she was showing the strain
of a woman’s mouth aching to be heard
among absent sheep in the halls of wool and tweed. My lips
reach toward hers—all these decades late—not to hunt
blood, gather and keep the sucrose-dappled bones
of bees, but to touch their fierce
flow somehow inside both the woman’s sound and her
intelligent mouth. She is more beautiful than winter
weight. Than the woods emptying the night. Empty
of night. Than shagbark hickory peeling back parts of itself
in layers of healing and hurt. She is beautiful
because she knows the deep soils
of the self and speaks among all that seek
to silence her, able to articulate the hidden
trace of trees. And the soft skin of her brain
tells me we could be partners, equal
among the plants, decoding the mystery
skin of the sea in one another’s ache. Let us tongue
every kind thought we’ve ever had, I think. Every thought
that stills the sorrow place. And in the doing, undo
the round honeycomb of the throat. The urgent now of a quickening
vowel. The urgent now of our mouths. Her mouth,
the most beautiful vowel there is, remaking me
as it emerges, deep, from a darkening down and claims the world
in a voicing that is, for once, finely heard. And is, finally, hers.

