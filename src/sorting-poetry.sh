#!/usr/bin/env bash
set -euo pipefail
IFS=$' '

usage() {
  cat <<EOF
Usage: ./$(basename "$0") [-h] [-V] 
Options:

-h, --help      Print this help and exit
-V, --version   Print version
Sample:
./sorting-poetry.sh data/a-valentine.md > ../dist/a-valentine.md
EOF
  exit
}

version() {
  cat <<EOF
v0.3.2
EOF
  exit 0
}

msg() {
  echo >&2 -e "${1-}"
}

errr() {
  local msg=$1
  local code=${2-1} # default exit status 1
  msg "$msg"
  exit "$code"
}

processfile() {
  local srcfile=$1
  echo "-------------------------------"
  echo "       concrete $srcfile       "
  echo "-------------------------------"
  [[ ! -e "$srcfile" ]] && errr "file 404"  
  sed -e 's/\.//g' -e 's/\,//g' -e 's/ /\n/g' "$srcfile" | tr '[:upper:]' '[:lower:]' | sort | uniq -c | sort -nr > _dat.md
  < _dat.md awk '{$1=$1;print}' > _tdat.md
while IFS=\  read -r qty word; do
  if [[ -n $word ]]; then
    echo ""
    start=1
    end=$qty
    for ((j=start; j<=end; j++))
      do
        printf "%0.s$word "
    done
    echo ""
  fi
done < _tdat.md
}

process() {
  for i in ${args[*]-}
  do
    processfile "$i"
  done
  exit 0
}

parse_params() {
  while :; do
    case "${1-}" in
    -h | --help) usage ;;
    -V | --version) version ;;
    *) break ;;
    esac
    shift
  done
  args=("$@")
  [[ ${#args[@]} -eq 0 ]] && errr "Provide a file to sort"
  return 0
}

parse_params "$@"
process

# EOF
