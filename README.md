# Concrete poet

Concrete poetry generator inspired by an interview with [Carl André found on Ubuweb](https://www.ubu.com/sound/andre.html). 
(I know Carl Andre is controversial.)

![screenshot A](src/img/a-valentine.md.png)


## Getting Started

Basicly you only need the file sorting-poetry.sh

bash
```
./sorting-poetry.sh -h # get help 
./sorting-poetry.sh -V # get version
./sorting-poetry.sh data/a-valentine.md > ../dist/a-valentine.md
```


### Prerequisites

You need one or more poems as input, in order to sort the words and generate a new concrete poem. 
In this repo, the sample [Poem: Greek Title](src/data/greek-title.md) is used. A poem by 
[By Oscar Fingal O'Flahertie Wills Wilde](https://www.public-domain-poetry.com/oscar-wilde/poem-greek-title-36254).

Or get a [Public Domain poem](https://www.public-domain-poetry.com/)

Generate a concrete poem:

```
./sorting-poetry.sh data/greek-title.md > ../dist/greek-title-concrete.md
```

Convert md to html
```
pandoc -s a-valentine.md -o a-valentine.html
```


## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://gitlab.com/Tankred/concrete-poet/-/tags). 

## Authors

Tankred

## License

This project is licensed under the GPL-3.0-or-later License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Hat tip to anyone whose code was used and all concrete poets
* [UBUWEB:sound - Carl Andre | U.S.(b. 1935)](https://www.ubu.com/sound/andre.html)
* [George Kalamaras, alligatorzine n 210](http://alligatorzine.be/pages/201/zine210.html).
* [gNoetry](https://gnoetrydaily.wordpress.com/page/4/)
* [Concrete poetry of Carl Andre](https://textinart.wordpress.com/2012/10/25/concrete-poetry-of-carl-andre/)
* [Ana Mendieta](https://www.theguardian.com/artanddesign/2013/sep/22/ana-mendieta-artist-work-foretold-death)
* [Miccaman on tilde.club](https://tilde.club/~miccaman/concretepoetry/a-valentine.html)
* [Concrete poem - greek tea](https://tilde.club/~miccaman/concretepoetry/greek-tea.html)
* [Poem: Greek Title By Oscar Wilde](https://www.public-domain-poetry.com/oscar-wilde/poem-greek-title-36254)
* [Font Acari Sans](https://fontlibrary.org/en/font/acari-sans)

