# Changelog                                                                  
                                                                              
All notable changes to this project will be documented in this file.        
                                                                              
The format is based on Keep a Changelog                                     
https://keepachangelog.com/en/1.0.0/, and this project adheres to Semantic  
Versioning https://semver.org/spec/v2.0.0.html.                             
                                                                              
## [Unreleased]                                                             
                                                                              
## [Released]                                                               
                                                                              
## [0.8.1] - 2024-11-08
### Changed
- Add a note on Carl Andre
                                                                              
## [0.8.0] - 2024-04-01
### Changed
- Quick, Quick, the Most Beautiful Vowel is Voiding
                                                                              
## [0.7.4] - 2024-02-03
### Changed
- WIP: Quick, Quick, the Most Beautiful Vowel is Voiding
                                                                              
## [0.7.3] - 2024-01-14
### Changed
- MVP: Quick, Quick, the Most Beautiful Vowel is Voiding
                                                                              
## [0.7.2] - 2024-01-07
### Changed
- Do not ignore dist/
### Added
- dist/a-valentine.html
- dist/a-valentine.md
- dist/greek-tea.md
- dist/greek-title-concrete.html
- dist/greek-title-concrete.md
- dist/sorted-quick.html
- dist/sorted-quick.md 
                                                                              
## [0.7.1] - 2023-08-05
### Added
- Meta data on 'Quick, Quick, the Most Beautiful Vowel is Voiding'
                                                                              
## [0.7.0] - 2023-07-28
### Changed
- Merge concrete poetry (Greek tea)
                                                                              
## [0.6.2] - 2023-07-10
### Changed
- Publish concrete poetry (Greek tea)
                                                                              
## [0.6.1] - 2023-06-25
### Changed
- Publish concrete poetry (convert md to html using pandoc)
- Publish on tilde club

## [0.6.0] - 2021-05-22
### Changed
- Bump version to 0.6.0

## [0.5.2] - 2021-05-20
### Changed
- Add extra spaces 

## [0.5.1] - 2021-05-16                                                     
### Changed
- Add notes on Carl Andre and Ana Mendieta

## [0.5.0] - 2021-05-13                                                     
### Changed
- Bump version to 0.5.0

## [0.3.1] - 2021-05-07                                                     
### Added
- Screenshot of output

## [0.3.0] - 2021-04-25                                                     
### Changed
- Bump version to 0.3.0

## [0.2.1] - 2021-04-25                                                     
### Changed
- Add details about poem used as input

## [0.2.0] - 2021-04-25                                                     
### Added                                                                   
- Core sorting poetry engine
### Changed
- Readme
- Ignore _ md files

## [0.1.0] - 2021-04-25                                                     
### Added                                                                   
- This CHANGELOG file
- A README file
- A minimal git ignore file
